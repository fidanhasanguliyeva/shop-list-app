import { COLORS } from "./colors";

export const headerStyle = {
    headerStyle: {
        backgroundColor: COLORS.PRIMARY,
        elevation: 0,
        shadowColor: "transparent",
        borderWidth: 0,
      },
      headerTintColor: "#fff",
      headerTitleAlign: "center",
      headerTitleStyle: {
        fontFamily: "MontserratMedium",
        fontSize: 16,
      }
}
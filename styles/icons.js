import back from "../assets/icons/back.png";
import menu from "../assets/icons/menu.png";
import edit from "../assets/icons/pen.png";
import remove from "../assets/icons/remove.png";
import save from "../assets/icons/save.png";
import user from "../assets/icons/user.jpg";

export const ICONS = {
  back,
  menu,
  edit,
  remove,
  save,
  user,
};

export const COLORS={
    PRIMARY: '#FF7676',
    SECONDARY: '#FFD976',
    ITEM_SECONDARY: '#FFE194',
    textColor: '#303234',
    backgroundColor: '#EEEEEE'
}
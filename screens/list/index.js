import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";

import { Container } from "../../commons";
import { ProductsList } from "./ProductsList";
import {
  getList,
  getData,
  resetProduct,
  toggleProduct,
  addProduct,
  updateProduct,
  deleteProduct,
} from "../../redux/data";

import { ListForm } from "./ListForm";
import { ListHeader } from "./ListHeader";

const mapStateToProps = (state, { route }) => ({
  users: getData(state),
  list: getList(state, route.params.listID),
});
const initalState = {
  onEdit: false,
  product: {},
};
export const ListScreen = connect(mapStateToProps, {
  getList,
  addProduct,
  updateProduct,
  deleteProduct,
  toggleProduct,
  resetProduct,
})(
  ({
    route,
    list,
    addProduct,
    updateProduct,
    deleteProduct,
    toggleProduct,
    resetProduct,
  }) => {
    const { listID, editMode, type } = route.params;
    const [productOnEdit, setProductOnEdit] = useState(initalState);

    const toggleToEdit = (item) => {
      setProductOnEdit({ onEdit: true, product: item });
    };

    const handleAdd = (product) => {
      addProduct({ listID, ...product });
    };
    const handleUpdate = (product) => {
      updateProduct({ listID, ...product });
    };
    const handleToggle = (productID) => {
      toggleProduct({ listID, productID });
    };
    const handleDelete = (productID) => {
      deleteProduct({ listID, productID });
    };
    const handleReset = () => {
      resetProduct(listID);
    };
    const cancelEditProduct = () => {
      setProductOnEdit(initalState);
    };
    return (
      <Container>
        {!editMode && (
          <ListHeader type={type} list={list} resetHandler={handleReset} />
        )}
        {editMode && (
          <ListForm
            productOnEdit={productOnEdit}
            isEditMode={productOnEdit.onEdit}
            listID={listID}
            addProduct={handleAdd}
            updateProduct={handleUpdate}
            cancelEditProduct={cancelEditProduct}
          />
        )}

        {list.map((item) => (
          <ProductsList
          key={item.id}
            productOnEditID={productOnEdit.product.id}
            editMode={editMode}
            products={item.products}
            deleteProduct={handleDelete}
            toggleProduct={handleToggle}
            toggleToEdit={toggleToEdit}
          />
        ))}
      </Container>
    );
  }
);

import React from "react";
import { StyleSheet, FlatList } from "react-native";

import { ProductItem } from "./ProductItem";
import { GLOBAL_STYLES } from "../../styles/globalStyles";

export const ProductsList = ({
  products,
  editMode,
  deleteProduct,
  toggleProduct,
  productOnEditID,
  toggleToEdit,
  }) => {
  return (
    <FlatList
      contentContainerStyle={[styles.list, { paddingTop: editMode ? 33 : 15 }]}
      data={products}
      keyExtractor={(item)=> item.id}
      renderItem={({ item }) => (
        <ProductItem
        key={item.id}
          isOnEdit={productOnEditID == item.id}
          onPressEdit={() => toggleToEdit(item)}
          onPressDelete={() => deleteProduct(item.id)}
          onLongPress={() => toggleProduct(item.id)}
          editMode={editMode}
          {...item}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  list: {
    paddingHorizontal: GLOBAL_STYLES.PADDING,
  },
});

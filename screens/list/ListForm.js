import React, { useState, useEffect } from "react";
import { StyleSheet, View, Alert } from "react-native";

import { CustomRadioBtn, CustomBtn, Field, CountField } from "../../components";
import { getWidth } from "../../utils/getWidth";
import { COLORS } from "../../styles/colors";

const options = ["pkg", "kg", "litre", "bott"];
const initialField = {
  name: "",
  count: 1,
  countType: options[0],
};

export const ListForm = ({
  addProduct,
  updateProduct,
  cancelEditProduct,
  productOnEdit,
}) => {
  const [fields, setFields] = useState(initialField);

  const handleFieldChange = (name, value) => {
    setFields((fields) => ({
      ...fields,
      [name]: value,
    }));
  };

  const addHandler = () => {
    if (fields.name.trim() !== "" && fields.name.length > 2) {
      addProduct({ product: fields });
      setFields(initialField);
    } else {
      Alert.alert("This is too short for name", "Please, try again");
    }
  };
  const editHandler = () => {
    if (fields.name.trim() !== "" && fields.name.length > 2) {
      updateProduct({ product: fields });
      cancelHandler();
    } else {
      Alert.alert("This is too short for name", "Please, try again");
    }
  };
  const cancelHandler = () => {
    setFields(initialField);
    cancelEditProduct();
  };
  useEffect(() => {
    if (productOnEdit.onEdit) {
      setFields(productOnEdit.product);
    }
  }, [productOnEdit]);
  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <Field
          value={fields.name}
          onChangeText={(v) => handleFieldChange("name", v)}
          title="position name"
          width={getWidth(75, 3)}
        />
        <CountField
          width={getWidth(25, 3)}
          value={fields.count}
          title="count"
          onChangeText={(v) => {
            handleFieldChange("count", v);
          }}
        />
      </View>
      <CustomRadioBtn
        onValueChange={(v) => {
          handleFieldChange("countType", v);
        }}
        style={styles.radio}
        options={options}
        value={fields.countType}
      />

      {productOnEdit.onEdit ? (
        <View style={styles.row}>
          <CustomBtn
            title="cancel"
            style={styles.cancel}
            width={getWidth(50, 3)}
            onPress={() => cancelHandler()}
          />
          <CustomBtn
            title="update"
            onPress={() => editHandler()}
            width={getWidth(50, 3)}
          />
        </View>
      ) : (
        <CustomBtn title="add to list" onPress={() => addHandler()} />
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingBottom: 21,
    borderBottomWidth: 2,
    borderBottomColor: COLORS.backgroundColor,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  radio: {
    marginVertical: 12,
  },
  cancel: {
    opacity: 0.5,
  },
});

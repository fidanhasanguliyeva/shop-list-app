import React, { useState } from "react";
import { StyleSheet, View, Alert } from "react-native";
import { connect } from "react-redux";

import { CustomBtn, Field } from "../components";
import { Container } from "../commons";
import { getData, updateUser } from "../redux/data";
import { GLOBAL_STYLES } from "../styles/globalStyles";

const mapStateToProps = (state) => ({
  users: getData(state),
});
export const UserSettings = connect(mapStateToProps, { updateUser })(
  ({ users, updateUser }) => {
    const name = users.map((user) => user.username)[0];
    const avatar = users.map((user) => user.avatarUri)[0];
    const [fields, setFields] = useState({
      name,
      avatar,
    });
    const handleUpdate = () => {
      if (fields.name.trim() !== "" && fields.name.length > 1) {
        updateUser(fields);
      } else {
        Alert.alert("This is too shirt for name", "Try again");
      }
    };
    const handleFieldChange = (field, value) => {
      setFields((fields) => ({
        ...fields,
        [field]: value,
      }));
    };

    return (
      <Container>
        <View style={styles.form}>
          <Field
            style={styles.field}
            title="name"
            value={fields.name}
            onChangeText={(e) => {
              handleFieldChange("name", e);
            }}
          />
          <Field
            style={styles.field}
            title="avatar"
            value={fields.avatar}
            onChangeText={(e) => {
              handleFieldChange("avatar", e);
            }}
          />
          <CustomBtn
            style={styles.btn}
            title="save changes"
            onPress={() => {
              handleUpdate();
            }}
          />
        </View>
      </Container>
    );
  }
);

const styles = StyleSheet.create({
  form: {
    marginTop: 10,
    flexDirection: "column",
    justifyContent: "space-between",
    width: "100%",
    paddingHorizontal: GLOBAL_STYLES.PADDING,
  },
  field: {
    fontFamily: "MontserratBold",
    fontSize: 14,
    textAlign: "center",
    height: 40,
    width: "100%",
    borderRadius: 20,

    marginBottom: 8,
  },
  btn: {
    marginTop: 8,
  },
});

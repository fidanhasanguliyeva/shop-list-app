import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";
import { connect } from "react-redux";

import { CustomBtn, CustomRadioBtn, Field } from "../components";
import { Container } from "../commons";
import { addList } from "../redux/data";
import { getWidth } from "../utils/getWidth";
import { createID } from "../utils/createID";
import { GLOBAL_STYLES } from "../styles/globalStyles";

const radioOptions = ["One Time", "Regular"];
const initialField = {
  name: "",
  type: radioOptions[0],
};
export const CreateList = connect(null, { addList })(
  ({ navigation, addList }) => {
    const [fields, setFields] = useState(initialField);

    const addHandler = () => {
      const listID = createID();
      if (fields.name.trim() !== "" && fields.name.length > 2) {
        addList({ listID, ...fields });
        navigation.navigate("List", {
          listID,
          name: fields.name,
          type: fields.type,
          editMode: true,
        });
        setFields(initialField);
      } else {
        Alert.alert("This is too short for name", "Please, try again");
      }
    };

    const handleFieldChange = (name, value) => {
      setFields((fields) => ({
        ...fields,
        [name]: value,
      }));
    };

    return (
      <Container>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.form}>
            <Field
              style={styles.field}
              title="name"
              value={fields.name}
              onChangeText={(e) => {
                handleFieldChange("name", e);
              }}
            />
            <CustomRadioBtn
              style={styles.field}
              value={fields.type}
              width={getWidth(50, 2)}
              selectedValue={fields.type}
              onValueChange={(val) => {
                handleFieldChange("type", val);
              }}
              options={radioOptions}
            />
            <CustomBtn
              onPress={() => {
                addHandler();
              }}
              title={"create list"}
            />
          </View>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
);
const styles = StyleSheet.create({
  form: {
    flexDirection: "column",
    justifyContent: "space-between",
    width: "100%",
    paddingHorizontal: GLOBAL_STYLES.PADDING,
  },
  field: {
    marginBottom: 15,
  },
});

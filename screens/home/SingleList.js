import React from "react";
import { View, StyleSheet, TouchableOpacity, FlatList } from "react-native";

import { CustomText } from "../../components/CustomText";
import { COLORS } from "../../styles/colors";

export const SingleList = ({ listType, onLongPress, lists, onPress }) => {
  const checkCompleted = (item) =>
    !!item.products.length &&
    item.products.filter((item) => item.completed === true).length /
      item.products.length;

  const oneTimeList =
    !!lists.length && lists.filter((item) => item.type == "One Time");
  const regularList =
    !!lists.length && lists.filter((item) => item.type == "Regular");

  let filteredList = listType == "One Time" ? oneTimeList : regularList;

  return (
    <FlatList
      data={filteredList}
      keyExtractor={(item) => item.id}
      renderItem={({ item }) => (
        <View
          key={item.id}
          style={
            listType == "One Time"
              ? !!item.products.length &&
                item.products.every((item) => item.completed == true)
                ? { opacity: 0.5 }
                : {}
              : {}
          }
        >
          <TouchableOpacity
            style={styles.itemWrapper}
            onPress={() => onPress(item)}
            onLongPress={() => onLongPress(item.id)}
          >
            <View style={styles.textWrapper}>
              <CustomText style={styles.itemName} weight="bold">
                {item.name}
              </CustomText>
              <CustomText style={styles.itemCount} weight="bold">
                {item.products.filter((item) => item.completed === true).length}
                /{item.products.length}
              </CustomText>
            </View>
            <View style={styles.progressBar}>
              <View
                style={[
                  {
                    width: `${checkCompleted(item) * 100}%`,
                  },
                  styles.progress,
                ]}
              ></View>
            </View>
          </TouchableOpacity>
        </View>
      )}
    />
  );
};

const styles = StyleSheet.create({
  itemWrapper: {
    borderWidth: 2,
    borderColor: COLORS.SECONDARY,
    borderRadius: 10,
    paddingTop: 11,
    paddingHorizontal: 20,
    paddingBottom: 15,
    marginBottom: 14,
  },
  textWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  itemName: {
    fontSize: 17,
  },
  itemCount: {
    fontSize: 14,
  },
  progressBar: {
    borderRadius: 13,
    marginTop: 7,
    marginBottom: 8,
    height: 19,
    overflow: "hidden",
    backgroundColor: COLORS.backgroundColor,
  },
  progress: {
    backgroundColor: COLORS.SECONDARY,
    height: 19,
    overflow: "hidden",
    borderRadius: 13,
  },
});

import { AsyncStorage } from "react-native";

export const INIT = "INIT";

export async function saveToAS(store) {
  const state = store.getState();
  await AsyncStorage.setItem("Data", JSON.stringify(state));
}

export async function getDataAS(store) {
  const res = await AsyncStorage.getItem("Data");
  if (res) {
    const data = JSON.parse(res);

    store.dispatch({
      type: INIT,
      payload: data,
    });
  }
}

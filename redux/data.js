import { INIT } from "./storeDataAS";
import { createID } from "../utils/createID";

//Action types
const UPDATE_USER = "UPDATE_USER";
const ADD_LIST = "ADD_LIST";
const DELETE_LIST = "DELETE_LIST";
const ADD_PRODUCT = "ADD_PRODUCT";
const UPDATE_PRODUCT = "UPDATE_PRODUCT";
const TOGGLE_PRODUCT = "TOGGLE_PRODUCT";
const RESET_PRODUCT = "RESET_PRODUCT";
const DELETE_PRODUCT = "DELETE_PRODUCT";

const MODULE_NAME = "data";
export const getData = (state) => state[MODULE_NAME].users;
export const getList = (state, id) =>
  getData(state).map((item) => item.lists.find((item) => item.id == id));

const initialState = {
  users: [
    {
      username: "John",
      avatarUri: "",
      id: createID(),
      lists: [
        {
          id: createID(),
          name: "Everything for breakfast",
          type: "Regular",
          products: [
            {
              id: createID(),
              name: "Pasta",
              count: "2",
              countType: "pkg",
              completed: false,
            },
            {
              id: createID(),
              name: "Salt",
              count: "2",
              countType: "pkg",
              completed: false,
            },
            {
              id: createID(),
              name: "Tomatoes",
              count: "2",
              countType: "kg",
              completed: true,
            },
            {
              id: createID(),
              name: "Cheese",
              count: "2",
              countType: "pkg",
              completed: false,
            },
          ],
        },
      ],
    },
  ],
};

//Reducer
export function dataReducer(state = initialState, { type, payload }) {
  switch (type) {
    case INIT:
      return {
        ...state,
        ...payload.data,
      };
    case UPDATE_USER:
      return {
        ...state,
        users: state.users.map((user) => ({
          ...user,
          username: payload.name,
          avatarUri: payload.avatar,
        })),
      };
    case ADD_LIST:
      return {
        ...state,
        users: state.users.map((item) => ({
          ...item,
          lists: [
            {
              id: payload.listID,
              name: payload.name,
              type: payload.type,
              products: [],
            },
            ...item.lists,
          ],
        })),
      };
    case DELETE_LIST:
      return {
        ...state,
        users: state.users.map((item) => ({
          ...item,
          lists: item.lists.filter((list) => list.id !== payload),
        })),
      };
    case ADD_PRODUCT:
      return {
        ...state,
        users: state.users.map((item) => ({
          ...item,
          lists: item.lists.map((list) => {
            if (list.id == payload.listID) {
              return {
                ...list,
                products: [
                  {
                    id: createID(),
                    name: payload.product.name,
                    count: payload.product.count,
                    countType: payload.product.countType,
                    completed: false,
                  },
                  ...list.products,
                ],
              };
            }
            return list;
          }),
        })),
      };
    case UPDATE_PRODUCT:
      console.log(payload, "payload");
      return {
        ...state,
        users: state.users.map((item) => ({
          ...item,
          lists: item.lists.map((list) => {
            if (list.id == payload.listID) {
              return {
                ...list,
                products: list.products.map((item) => {
                  if (item.id == payload.product.id) {
                    return {
                      ...item,
                      name: payload.product.name,
                      count: payload.product.count,
                      countType: payload.product.countType,
                    };
                  }
                  return item;
                }),
              };
            }
            return list;
          }),
        })),
      };
    case DELETE_PRODUCT:
      return {
        ...state,
        users: state.users.map((item) => ({
          ...item,
          lists: item.lists.map((list) => {
            if (list.id == payload.listID) {
              return {
                ...list,
                products: list.products.filter(
                  (product) => product.id !== payload.productID
                ),
              };
            }
            return list;
          }),
        })),
      };

    case TOGGLE_PRODUCT:
      return {
        ...state,
        users: state.users.map((item) => ({
          ...item,
          lists: item.lists.map((list) => {
            if (list.id == payload.listID) {
              return {
                ...list,
                products: list.products.map((item) => {
                  if (item.id == payload.productID) {
                    return {
                      ...item,
                      completed: true,
                    };
                  }
                  return item;
                }),
              };
            }
            return list;
          }),
        })),
      };
    case RESET_PRODUCT:
      return {
        ...state,
        users: state.users.map((item) => ({
          ...item,
          lists: item.lists.map((item) => {
            if (item.id == payload) {
              return {
                ...item,
                products: item.products.map((item) => ({
                  ...item,
                  completed: false,
                })),
              };
            }
            return item;
          }),
        })),
      };
    default:
      return state;
  }
}

//Action creators
export const updateUser = (payload) => ({
  type: UPDATE_USER,
  payload,
});
export const addList = (payload) => ({
  type: ADD_LIST,
  payload,
});

export const deleteList = (payload) => ({
  type: DELETE_LIST,
  payload,
});
export const addProduct = (payload) => ({
  type: ADD_PRODUCT,
  payload,
});
export const updateProduct = (payload) => ({
  type: UPDATE_PRODUCT,
  payload,
});
export const toggleProduct = (payload) => ({
  type: TOGGLE_PRODUCT,
  payload,
});
export const deleteProduct = (payload) => ({
  type: DELETE_PRODUCT,
  payload,
});
export const resetProduct = (payload) => ({
  type: RESET_PRODUCT,
  payload,
});



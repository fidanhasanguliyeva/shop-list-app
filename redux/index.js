import { createStore, combineReducers, applyMiddleware } from "redux";
import { dataReducer } from "./data";


import { saveToAS, getDataAS } from "./storeDataAS";
const rootReducer = combineReducers({
  data: dataReducer,
});

const store = createStore(rootReducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

store.subscribe(() => saveToAS(store));
getDataAS(store);
export default store;

import React from "react";
import { StyleSheet, View, TextInput } from "react-native";

import { CustomText } from "./CustomText";
import { COLORS } from "../styles/colors";
import { font_families } from "../styles/fonts";

export const Field = ({
  title,
  width,
  style,
  ...rest
}) => {
  return (
    <View style={{ width }}>
      <CustomText weight="semi" style={styles.title}>
        {title}
      </CustomText>
      <TextInput
        {...rest}
        
        style={[styles.field, style]}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  title: {
    opacity: 0.75,
    textAlign: "center",
    fontSize: 12,
    color: COLORS.textColor,
  },
  field: {
    paddingHorizontal: 15,
    marginTop: 7,
    fontFamily: font_families.bold,
    fontSize: 14,
    textAlign: "center",
    height: 43,
    backgroundColor: COLORS.backgroundColor,
    borderRadius: 25,
  },
});

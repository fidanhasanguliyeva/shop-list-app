import React from "react";
import { TouchableOpacity, StyleSheet, View } from "react-native";

import { CustomText } from "./CustomText";
import { COLORS } from "../styles/colors";

export const CustomBtn = ({ onPress, title, style, width, fontSize }) => {
  return (
    <TouchableOpacity style={{ width }} onPress={onPress}>
      <View style={[styles.btn, style]}>
        <CustomText
          weight="bold"
          style={{ ...styles.btnTitle, fontSize: fontSize || 14 }}
        >
          {title}
        </CustomText>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 25,
    height: 42,
    backgroundColor: COLORS.PRIMARY,
  },
  btnTitle: {
    color: "white",
    textTransform: "uppercase",
    fontSize: 14,
  },
});

import React from "react";
import { StyleSheet, TouchableOpacity, Image } from "react-native";
import { ICONS } from "../styles/icons";

export const HeaderIcon = ({ iconName, onPress }) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Image style={styles.icon} source={ICONS[iconName] || ""} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 15,
    width: 22,
    height: 22,
  },
  icon: {
    width: "100%",
    height: "100%",
  },
});

import React from "react";
import { StyleSheet, View, TouchableOpacity, Dimensions } from "react-native";

import { COLORS } from "../styles/colors";
import { CustomText } from "./CustomText";
import { getWidth } from "../utils/getWidth";

export const CustomRadioBtn = ({ options, value, onValueChange, style }) => (
  <View style={[styles.container, style]}>
    {options.map((option) => {
      const isSelected = value === option;
      return (
        <TouchableOpacity key={option} onPress={() => onValueChange(option)}>
          <View
            key={option}
            style={[
              styles.radioBtn,
              {
                opacity: isSelected ? 1 : 0.5,
                width: getWidth(100 / options.length, 2 + options.length - 1),
              },
            ]}
          >
            <CustomText style={isSelected ? { fontWeight: "bold" } : {}}>
              {option}
            </CustomText>
          </View>
        </TouchableOpacity>
      );
    })}
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  radioBtn: {
    height: 42,
    backgroundColor: COLORS.backgroundColor,
   
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
  },
});

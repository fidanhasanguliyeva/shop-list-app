import React from "react";
import { Text } from "react-native";

import { font_families } from "../styles/fonts";

export const CustomText = ({ children, weight, style, ...rest }) => {
  return (
    <Text
      style={{
        fontFamily: font_families[weight] || font_families.regular,
        ...style,
      }}
      {...rest}
    >
      {children}
    </Text>
  );
};

import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { HomeStack } from "./HomeStack";
import { CustomDrawer } from "../commons/CustomDrawer";
import { CreateStack } from "./CreateStack";
import { UserSettingsStack } from "./UserSettingsStack";

const { Navigator, Screen } = createDrawerNavigator();

export const RootDrawer = () => {
  return (
    <NavigationContainer>
      <Navigator
        drawerStyle={{
          width: 280,
        }}
        drawerContent={(props) => <CustomDrawer {...props} />}
        initialRouteName={"OneTimeLists"}
      >
        <Screen name="CreateList" component={CreateStack} />
        <Screen name="OneTimeLists" component={HomeStack} />
        <Screen name="UserSettings" component={UserSettingsStack} />
      </Navigator>
    </NavigationContainer>
  );
};

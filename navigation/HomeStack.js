import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { HeaderIcon } from "../components/HeaderIcon";
import { HomeScreen } from "../screens";

import { headerStyle } from "../styles/headerStyle";
import { ListScreen } from "../screens/list";

const { Navigator, Screen } = createStackNavigator();

export const HomeStack = () => {
  return (
    <Navigator screenOptions={headerStyle} 
    >
      <Screen
        initialParams={{ type: "One Time" }}
        name="Home"
        component={HomeScreen}
        options={({ navigation, route }) => ({
          title: `${route?.params?.type} Lists`,

          headerRight: () => (
            <HeaderIcon iconName="menu" onPress={navigation.toggleDrawer} />
          ),
        })}
      />

      <Screen
        name="List"
        component={ListScreen}
        options={({ navigation, route }) => ({
          title: route.params.title,
          headerLeft: () => (
            <HeaderIcon iconName="back" onPress={() => navigation.navigate("Home", {type: route.params.type})} />
          ),
          headerRight: () => (
            <HeaderIcon
              iconName={route.params?.editMode ? "save" : "edit"}
              onPress={() => {
                navigation.setParams({ editMode: !route.params?.editMode });
              }}
            />
          ),
        })}
      />
      
    </Navigator>
  );
};

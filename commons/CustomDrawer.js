import React from "react";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { connect } from "react-redux";

import { CustomText } from "../components";
import { COLORS } from "../styles/colors";

import { initData, getData } from "../redux/data";
import { ICONS } from "../styles/icons";

const mapStateToProps = (state) => ({
  users: getData(state),
});

export const CustomDrawer = connect(mapStateToProps, { initData })(
  ({ users, navigation }) => {
    let name = users.map((user) => user.username)[0];
    let img = users.map((user) => user.avatarUri)[0];

    return (
      <View style={styles.container}>
        <View style={styles.heading}>
          <Image style={styles.img} source={img ? { uri: img } : ICONS.user} />
          <CustomText style={styles.title}>{name}</CustomText>
        </View>
        <View style={styles.content}>
          <TouchableOpacity
            style={styles.navigation}
            onPress={() => navigation.navigate("CreateList")}
          >
            <CustomText style={styles.navName}>ADD NEW LIST</CustomText>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.navigation, { marginTop: 30 }]}
            onPress={() => navigation.navigate("Home", { type: "One Time" })}
          >
            <CustomText style={styles.navName}>ONE TIME LIST</CustomText>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.navigation}
            onPress={() => navigation.navigate("Home", { type: "Regular" })}
          >
            <CustomText style={styles.navName}>REGULAR LIST</CustomText>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.navigation}
            onPress={() => navigation.navigate("UserSettings")}
          >
            <CustomText style={styles.navName}>USER SETTINGS</CustomText>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
  },
  heading: {
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 12,
    flexDirection: "row",
    justifyContent: "center",
  },
  title: {
    fontSize: 25,
    color: COLORS.textColor,
    paddingHorizontal: 20,
  },
  content: {
    backgroundColor: COLORS.PRIMARY,
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  navigation: {
    backgroundColor: "white",
    marginTop: 15,
    borderRadius: 25,
    height: 32,
  },
  navName: {
    fontSize: 17,
    color: COLORS.PRIMARY,
    fontFamily: "MontserratBold",
    paddingHorizontal: 15,
    paddingVertical: 4,
    textAlign: "center",
  },
  img: {
    width: 50,
    height: 50,
    borderWidth: 3,
    borderColor: COLORS.BG_PRIMARY,
    borderRadius: 25,
  },
});

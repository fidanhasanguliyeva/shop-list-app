import React, { useState } from "react";
import { AppLoading } from "expo";
import { Provider } from "react-redux";
import store from "./redux";
import { loadFonts } from "./styles/fonts";
import { RootDrawer } from "./navigation/RootDrawer";


export default function App() {
  const [loaded, setLoaded] = useState(false);

  if (!loaded) {
    return (
      <AppLoading
        startAsync={loadFonts}
        onFinish={()=>setLoaded(true)}
        onError={() => console.log("can not be loaded")}
      />
    );
  }

  return (
    <Provider store={store}>
     <RootDrawer /> 
    </Provider>
  );
}
